#Desafio FullStack C. S. F. Junior
##Site Blindado - B2W

###Instruções
* Criar virtual enviroment e ativá-lo:   
`$ python -m venv env`   
`$ source env/Scripts/activate`   

* Instalar dependências:   
`$ pip install -r requirements.txt`   

* Rodar aplicação:   
`$ cd src`   
`$ python -m manage.py runserver`   

* Criar usuário administrador:   
`$ cd src`   
`$ python manage.py createsuperuser --email admin@example.com --username admin`   

* Rodar aplicação:   
`$ python -m manage.py runserver`   

### Testar   
* Rodar testes automáticos (tests.py):   
`$ python -m manage.py test`   

from django.db import models

# Create your models here.
class Profiles(models.Model):
    #Nome do cliente
    nome = models.CharField(max_length=100, null=False)
    #Endereço
    end = models.TextField(max_length=255, null=False)
    #cpf
    cpf = models.CharField(max_length=11, null=False)
    #Email
    email = models.EmailField()
    #Telefone
    tel = models.CharField(max_length=13, null=False)
    #Domínios
    dom = models.TextField(max_length=510, null=False)

    def __str__(self):
        return "Cliente: {}\nEndereço: {}\nCPF: {}\nE-mail: {}\nTel.: {}\nDomínios: {}"\
        .format(self.nome, self.end, self.cpf, self.email, self.tel, self.dom)

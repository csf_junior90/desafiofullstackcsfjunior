from django.shortcuts import render
from rest_framework import generics
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import status
from .models import Profiles
from .serializers import ProfilesSerializer
from .decorators import validate_request_data

# Create your views here.
class ListCreateProfilesView(generics.ListCreateAPIView):
    """
    GET profiles/
    POST profiles/
    """
    queryset = Profiles.objects.all()
    serializer_class = ProfilesSerializer

    @validate_request_data
    def post(self, request, *args, **kwargs):
        a_profile = Profiles.objects.create(
            nome=request.data["nome"],
            end=request.data["end"],
            cpf=request.data["cpf"],
            email=request.data["email"],
            tel=request.data["tel"],
            dom=request.data["dom"]
        )
        return Response(
            data=ProfilesSerializer(a_profile).data,
            status=status.HTTP_201_CREATED
        )

class ProfilesDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    GET profiles/:id/
    PUT profiles/:id/
    DELETE profiles/:id/
    """
    queryset = Profiles.objects.all()
    serializer_class = ProfilesSerializer

    def get(self, request, *args, **kwargs):
        try:
            a_profile = self.queryset.get(pk=kwargs["pk"])
            return Response(ProfilesSerializer(a_profile).data)
        except Profiles.DoesNotExist:
            return Response(
                data={
                    "message": "Profile with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )

    @validate_request_data
    def put(self, request, *args, **kwargs):
        try:
            a_profile = self.queryset.get(pk=kwargs["pk"])
            serializer = ProfilesSerializer()
            updated_profile = serializer.update(a_profile, request.data)
            return Response(ProfilesSerializer(updated_profile).data)
        except Profiles.DoesNotExist:
            return Response(
                data={
                    "message": "Profile with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )

    def delete(self, request, *args, **kwargs):
        try:
            a_profile = self.queryset.get(pk=kwargs["pk"])
            a_profile.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Profiles.DoesNotExist:
            return Response(
                data={
                    "message": "Profile with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )

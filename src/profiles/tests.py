from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Profiles
from .serializers import ProfilesSerializer
import json

# tests for views

class BaseViewTest(APITestCase):
    apicli = APIClient()

    @staticmethod
    def create_profile(nome="", end="", cpf="", email="", tel="", dom=""):
        if nome != "" and end != "" and cpf != "" and email != "" and tel != "" and dom != "":
            Profiles.objects.create(nome=nome, end=end, cpf=cpf, email=email, tel=tel, dom=dom)

    def make_a_request(self, kind="post", **kwargs):
        """
        Make a post request to create a prfile
        :param kind: HTTP VERB
        :return:
        """
        if kind == "post":
            return self.client.post(
                reverse(
                    "profiles-list-create",
                    kwargs={
                        "version": kwargs["version"]
                    }
                ),
                data=json.dumps(kwargs["data"]),
                content_type='application/json'
            )
        elif kind == "put":
            return self.client.put(
                reverse(
                    "profiles-detail",
                    kwargs={
                        "version": kwargs["version"],
                        "pk": kwargs["id"]
                    }
                ),
                data=json.dumps(kwargs["data"]),
                content_type='application/json'
            )
        else:
            return None

    def fetch_a_profile(self, pk=0):
        return self.client.get(
            reverse(
                "profiles-detail",
                kwargs={
                    "version": "v1",
                    "pk": pk
                }
            )
        )

    def delete_a_profile(self, pk=0):
        return self.client.delete(
            reverse(
                "profiles-detail",
                kwargs={
                    "version": "v1",
                    "pk": pk
                }
            )
        )

    def setUp(self):
        # add test data
        self.create_profile("Holden Smith", "Rua Versalles 123", "00000000000", "ravi.h@mail.com", "99999-0000"\
        "www.smith.com, www.holden.com")
        self.create_profile("Sandra Rosa Madalena", "Rua Versalles 123", "11111111111", "srosa.m@mail.com", "99999-1111"\
        "www.rosa.com, www.madalena.com")
        self.create_profile("Countess Bathory", "Rua Claro 456", "22222222222", "countess@mail.com", "99999-2222"\
        "www.countess.com, www.bathory.com")
        self.create_profile("Zeca Silva", "Rua Anarco 789", "33333333333", "zeca@mail.com", "99999-3333"\
        "www.zeca.com, www.silva.com")

        self.valid_data = {
            "nome": "test name",
            "end": "Rua teste 456",
            "cpf": "11111111111",
            "email": "teste@mail.com",
            "tel": "99999-9999",
            "dom": "www.teste.com"
        }

        self.invalid_data = {
            "nome": "",
            "end": "",
            "cpf": "",
            "email": "",
            "tel": "",
            "dom": ""
        }

        self.valid_profile_id = 1
        self.invalid_profile_id = 100

#GET (All)
class GetAllProfilesTest(BaseViewTest):
    def test_get_all_profiles(self):
        """
        This test ensures that all clients added in the setUp method
        exist when we make a GET request to the clients/ endpoint
        """
        # hit the API endpoint
        response = self.apicli.get(
            reverse("profiles-list-create", kwargs={"version": "v1"})
        )
        # fetch the data from db
        expected = Profiles.objects.all()
        serialized = ProfilesSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

#GET (Single Profile)
class GetASingleProfileTest(BaseViewTest):
    def test_get_a_profile(self):
        """
        This test ensures that a single profile of a given id is
        returned
        """
        # hit the API endpoint
        response = self.fetch_a_profile(self.valid_profile_id)
        # fetch the data from db
        expected = Profiles.objects.get(pk=self.valid_profile_id)
        serialized = ProfilesSerializer(expected)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # test with a song that does not exist
        response = self.fetch_a_profile(self.invalid_profile_id)
        self.assertEqual(
            response.data["message"],
            "Profile with id: 100 does not exist"
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

#POST
class AddProfilesTest(BaseViewTest):
    def test_create_a_profile(self):
        """
        This test ensures that a single profile can be added
        """
        # hit the API endpoint
        response = self.make_a_request(
            kind="post",
            version="v1",
            data=self.valid_data
        )
        self.assertEqual(response.data, self.valid_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # test with invalid data
        response = self.make_a_request(
            kind="post",
            version="v1",
            data=self.invalid_data
        )
        self.assertEqual(
            response.data["message"],
            "All fields are required to add a profile"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#PUT
class UpdateProfilesTest(BaseViewTest):
    def test_update_a_profile(self):
        """
        This test ensures that a single profile can be updated. In this
        test we update the second profile in the db with valid data and
        the third profile with invalid data and make assertions
        """
        # hit the API endpoint
        response = self.make_a_request(
            kind="put",
            version="v1",
            id=2,
            data=self.valid_data
        )
        self.assertEqual(response.data, self.valid_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # test with invalid data
        response = self.make_a_request(
            kind="put",
            version="v1",
            id=3,
            data=self.invalid_data
        )
        self.assertEqual(
            response.data["message"],
            "All fields are required to update a profile"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#DELETE
class DeleteProfilesTest(BaseViewTest):
    def test_delete_a_profile(self):
        """
        This test ensures that when a profile of given id can be deleted
        """
        # hit the API endpoint
        response = self.delete_a_profile(1)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # test with invalid data
        response = self.delete_a_profile(100)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

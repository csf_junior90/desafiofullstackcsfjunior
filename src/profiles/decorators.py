from rest_framework.response import Response
from rest_framework.views import status

def validate_request_data(fn):
    def decorated(*args, **kwargs):
        # args[0] == GenericView Object
        nome = args[0].request.data.get("nome", "")
        end = args[0].request.data.get("end", "")
        cpf = args[0].request.data.get("cpf", "")
        email = args[0].request.data.get("email", "")
        tel = args[0].request.data.get("tel", "")
        dom = args[0].request.data.get("dom", "")
        if not nome and not end and not cpf and not email and not tel\
        and not dom:
            return Response(
                data={
                    "message": "All fields are required to add a profile"
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        return fn(*args, **kwargs)
    return decorated

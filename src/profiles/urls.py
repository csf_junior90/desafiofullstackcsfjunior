from django.urls import path
from .views import ListCreateProfilesView, ProfilesDetailView
urlpatterns = [
    path('profiles/', ListCreateProfilesView.as_view(), name='profiles-list-create'),
    path('profiles/<int:pk>/', ProfilesDetailView.as_view(), name='profiles-detail')
]
